interface AnimalModel {
    name: string;
}

interface IAnimal {
    makeSound(): void;
    getName(): string;
}

class Animal2 {

}